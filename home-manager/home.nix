{ config, pkgs, inputs, ... }:

let
  gruvboxPlus = import ./gruvbox-plus.nix { inherit pkgs; };
in
{
  imports = [
    inputs.nix-colors.homeManagerModules.default
    ./programs/firefox
    ./programs/hyprland
    ./programs/kitty
    ./programs/neovim
    ./programs/waybar
    ./programs/wofi
    ./programs/zsh
  ];

  colorScheme = inputs.nix-colors.colorSchemes.gruvbox-dark-medium;
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "gearoid";
  home.homeDirectory = "/home/gearoid";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "22.11"; # Please read the comment before changing.

  qt.enable = true;
  qt.platformTheme = "qtct";
  #qt.platformTheme = "gtk";
  #qt.style.name = "adwaita-dark";
  #qt.style.package = pkgs.adwaita-qt;

  gtk.enable = true;
  
  gtk.cursorTheme.package = pkgs.bibata-cursors;
  gtk.cursorTheme.name = "Bibata-Modern-Ice";
  
  gtk.theme.package = pkgs.adw-gtk3;
  gtk.theme.name = "adw-gtk3";
  
  gtk.iconTheme.package = gruvboxPlus;
  gtk.iconTheme.name = "GruvboxPlus";

  fonts.fontconfig.enable = true;
  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    ruby_3_2
    nerdfonts
    bundix
    pulseaudio
    pavucontrol
    swayosd
    krita
    cinnamon.nemo
    lxappearance
    tree
  ];

  programs.bat.enable = true;
  programs.eza.enable = true;

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
  };

  home.file = {
    ".icons/bibata".source = "${pkgs.bibata-cursors}/share/icons/Bibata-Modern-Classic";
  };

  home.sessionVariables = {
    EDITOR = "nvim";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

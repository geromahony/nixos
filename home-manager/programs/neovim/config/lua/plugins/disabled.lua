return {
  -- disable flash
  { "folke/flash.nvim", enabled = false },
  { "williamboman/mason.nvim", enabled = false },
}

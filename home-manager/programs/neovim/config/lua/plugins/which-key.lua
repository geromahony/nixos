return {
  "folke/which-key.nvim",
  opts = {
    defaults = {
      ["<leader>h"] = { name = "+harpoon" },
      ["<leader>r"] = { name = "+rspec" },
      ["<leader>t"] = { name = "+terminal" },
    },
  },
}

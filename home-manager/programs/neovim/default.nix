{ config, pkgs, ... }:

{

  programs.neovim = {
    enable = true;

    plugins = with pkgs; [
      vimPlugins.mason-nvim
      vimPlugins.mason-lspconfig-nvim
      vimPlugins.nvim-treesitter.withAllGrammars
      vimPlugins.nvim-treesitter-textobjects
    ];

    extraPackages = with pkgs; [
      lua-language-server
      rubyPackages_3_2.solargraph
      rubyPackages_3_2.rubocop
      rubyPackages_3_2.minitest
    ];
  };

  xdg.configFile.nvim = {
    source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/.config/home-manager/programs/neovim/config";
    recursive = true;
  };

}

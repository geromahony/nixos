{ pkgs, user, lib, config, ... }:

{

  wayland.windowManager.hyprland = {
    enable = true;
    package = pkgs.hyprland;
    systemdIntegration = true;
    extraConfig = builtins.readFile ./hyprland.conf;
    enableNvidiaPatches = true;
    xwayland.enable = true;
  };

  home.file = {
    ".config/hypr/start.sh".source = ./start.sh;
    ".config/hypr/wallpapers/gruvbox-coffee.png".source = ./wallpapers/gruvbox-coffee.png;
  };

}


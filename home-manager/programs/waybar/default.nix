{ config, pkgs, ... }:

{
  programs = {

    waybar = {
      enable = true;
      style = builtins.readFile ./style.css;
      systemd = {
        enable = true;
        target = "sway-session.target";
      };

      settings = {

        mainBar = {

          layer = "top";
          position = "top";
          height = 40;
          spacing = 2;
          margin-bottom = -10;

          modules-left = [ "wlr/workspaces" ];
          modules-right = [ "cpu" "memory" "backlight" "pulseaudio" "network" "battery" "tray"];
          modules-center = [ "clock" ];

          "wlr/workspaces" = {
              format = "{icon}";
              format-active = "{icon}";
              on-click = "activate";
              on-scroll-up = "${pkgs.hyprland}/bin/hyprctl dispatch workspace e-1";
              on-scroll-down = "${pkgs.hyprland}/bin/hyprctl dispatch workspace e+1";
          };

          tray = {
            icon-size = 15;
            spacing = 5;
            #show-passive-items = false;
          };

          clock = {
            tooltip = false;
            interval = 60;
            format = "{:%H:%M}";
            max-length = 25;
            on-click = "${pkgs.kitty}/bin/kitty --class calendar -e calcure";
          };

          cpu = {
            interval = 15;
            format = "{}%  ";
            max-length = 10;
            on-click = "${pkgs.kitty}/bin/kitty --class system_monitor -e btop -p 2";
          };

          memory = {
            format = "{percentage}% 󰇖";
            interval = 1;
            max-lenght = 35;
            on-click = "${pkgs.kitty}/bin/kitty --class system_monitor -e htop";
          };
        
          backlight = {
            format = "{percent}% {icon}";
            on-scroll-down = "swayosd-client --brightness raise";
            on-scroll-up = "swayosd-client --brightness lower";
            format-icons = ["󰃚" "󰃚" "󰃛" "󰃜" "󰃜" "󰃝" "󰃞" "󰃟" "󰃠"];
          };

          battery = {
            states = {
              warning = 25;
              critical = 10;
            };
            format = "{capacity}% {icon}";
            format-charging = "{capacity}% 󰂅";
            format-plugged = "{capacity}% {icon}";
            format-good = ""; # An empty format will hide the module
            format-full = "󰁹";
            format-icons = ["󰂎" "󰁺" "󰁻" "󰁼" "󰁽" "󰁾" "󰁿" "󰂀" "󰂁" "󰂂" "󰁹"];
            #on-click = ".config/rofi/powermenu/type-5/powermenu.sh";
          };

          pulseaudio = {
            format = "{volume}% {icon}";
            format-bluetooth = "{volume}% {icon}";
            format-bluetooth-muted = "婢  muted";
            format-muted = "󰖁 muted";
            format-icons = {
              headphone = "󰋋";
              hands-free = "";
              headset = "󰋋";
              phone = "󰄜";
              portable = "";
              car = "󰸛";
              default = ["󰕿" "󰖀" "󰕾"];
            };
            on-click-right = "pavucontrol";
            on-click = "swayosd-client --output-volume mute-toggle";
            on-scroll-down = "swayosd-client --output-volume raise";
            on-scroll-up = "swayosd-client --output-volume lower";
          };

          network = {
            interface = "wlp3s0";
            format-wifi =  " {signalStrength}% 󰤨 ";
            format-disconnected = " Offline 󰤭 ";
            tooltip-format = "{ifname} via {gwaddr} 󰱶";
            tooltip-format-wifi = "{essid}";
            #on-click": "~/.config/hypr/scripts/rofi-wifi-menu";
          };

        };
      };
    };
  };
}

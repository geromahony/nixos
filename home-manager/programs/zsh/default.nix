{ config, pkgs, ... }:

{

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      lsa = "eza -alh --icons --git";
      hms = "home-manager switch";
    };
    history.size = 10000;
    history.path = "$HOME/.cache/zsh_history";
    initExtra = ''
      eval $(zoxide init zsh)
    '';
    oh-my-zsh = {
      enable = true;
      plugins = [ "git" ];
      custom = "${config.home.homeDirectory}/.config/home-manager/programs/zsh/custom";
      theme = "gruvbox";
    };
  };

}
